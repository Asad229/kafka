package com.asad.kafka.service;

import com.asad.kafka.util.AppConstants;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Service
public class KafkaConsumerService {

    private final Logger logger =
            LoggerFactory.getLogger(KafkaProducerService.class);

    @KafkaListener(topics = AppConstants.TOPIC_NAME,
            groupId = AppConstants.GROUP_ID)
    public void consume(String message)
    {
        logger.info(String.format("Message received -> %s", message));
    }

}
